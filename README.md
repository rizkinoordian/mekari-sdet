# README #

## What is this repository for? ##

This repository for recruitment task in mekari

## How do I get set up? ##

### Prerequisite ###

* Ruby 2.5.5
* git

### Setup ###

* Clone repository
  > git clone https://rizkinoordian@bitbucket.org/rizkinoordian/mekari-sdet
  > cd mekari-sdet
* Install bundler
  > gem install bundler
* Run bundle install
  > bundle install

### How to run the automation ###
* For web automation use tag `@amazon`, `@signin`, or `@signout`
  > cucumber --tags "@signin"  # for specific feature
* For api automation use tag `@api`
  > cucumber --tags "@api"
