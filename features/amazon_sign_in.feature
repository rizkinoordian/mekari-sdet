@amazon @signin
Feature: Amazon Sign In

Background: User visit Amazon sign in page
  Given a user visit Amazon home page
  And a user visit to Amazon sign in page

Scenario: User sign in using wrong email
  When a user fill email with "rizki.noordian@outlook.com"
  Then user should get an error message after click continue

Scenario: User sign in using wrong password
  When a user submit blank form
  Then user should get an error message in "email" field
