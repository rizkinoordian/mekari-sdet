@amazon @signup
Feature: Amazon Sign Up

Background: User visit Amazon home page
  Given a user visit Amazon home page
  And a user visit to Amazon sign in page
  And a user visit to Amazon sign up page

Scenario: User submit blank form
  When a user submit blank form
  Then user should get an error message in "name" field
  And user should get an error message in "email" field
  And user should get an error message in "password" field

Scenario: User sign up but re-enter password is not match
  When a user fill password with "awdawf1213" and re-enter password with "aaaaa"
  Then user should get an error message in "re-enter" field
