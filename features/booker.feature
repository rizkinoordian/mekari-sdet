@api
Feature: REST API Booker

Scenario: Client can delete booking
  When a client send POST request to "/booking"
  Then the response code should be "200"
  And the response should have body
  When a client to authorize with data:
  | username | admin       |
  | password | password123 |
  And a client send DELETE request to "/booking" 
  Then the response code should be "201"
  And the data should be deleted
