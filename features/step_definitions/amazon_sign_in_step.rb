Given(/^a user visit Amazon home page$/) do
  @browser.goto("https://www.amazon.com")
end

And(/^a user visit to Amazon sign in page$/) do
  nav_sign_in = @browser.a(:css => '#nav-link-accountList')
  nav_sign_in.wait_until(&:present?)
  nav_sign_in.click
end

When(/^a user fill email with "([^"]*)"$/) do |email|
  email_field = @browser.text_field(:css => '.auth-required-field')
  continue_btn = @browser.button(:css => '#continue')
  email_field.wait_until(&:present?)
  email_field.set "#{email}"
  continue_btn.click
end

Then(/^user should get an error message after click continue$/) do
  error_message = @browser.div(:css => '#auth-error-message-box')
  error_message.wait_until(&:present?)
end

And(/^a user fill password with "([^"]*)"$/) do |password|
  password_field = @browser.text_field(:css => '#ap_password')
  sign_in_btn = @browser.button(:css => '#signInSubmit')
  password_field.wait_until(&:present?)
  password_field.set "#{password}"
  sign_in_btn.click
end
