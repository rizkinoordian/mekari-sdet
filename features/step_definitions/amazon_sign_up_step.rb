And(/^a user visit to Amazon sign up page$/) do
  create_account_btn = @browser.a(:css => '#createAccountSubmit')
  create_account_btn.wait_until(&:present?)
  create_account_btn.click
end

When(/^a user submit blank form$/) do
  create_account_btn = @browser.button(:css => '#continue')
  create_account_btn.wait_until(&:present?)
  create_account_btn.click
end

Then(/^user should get an error message in "([^"]*)" field$/) do |field_name|
  case field_name
  when 'name'
    error_message = @browser.div(:css => '#auth-customerName-missing-alert')
  when 'email'
    error_message = @browser.div(:css => '#auth-email-missing-alert')
  when 'password'
    error_message = @browser.div(:css => '#auth-password-missing-alert')
  else
    error_message = @browser.div(:css => '#auth-password-mismatch-alert')
  end
  error_message.wait_until(&:present?)
end

When(/^a user fill password with "([^"]*)" and re-enter password with "([^"]*)"$/) do |password, re_enter_password|
  password_field = @browser.text_field(:css => '#ap_password')
  re_enter_field = @browser.text_field(:css => '#ap_password_check')
  create_account_btn = @browser.button(:css => '#continue')
  password_field.wait_until(&:present?)
  password_field.set "#{password}"
  re_enter_field.set "#{re_enter_password}"
  create_account_btn.click
end
