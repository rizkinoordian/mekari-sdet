When(/^a client to authorize with data:$/) do |account|
  account = account.rows_hash.to_json
  options = {
    headers: {"Content-Type" => "application/json"},
    body: account
  }
  puts "#{account}"
  @response = HTTParty.post("#{@api_url}/auth", options)
  @response = JSON.parse(@response.body)
end

When(/^a client send (DELETE|POST) request to "([^"]*)"$/) do |method, relative_path|
  case method
  when 'DELETE'
    headers = {
      headers: {"Content-Type" => "application/json", "Cookie" => "token=#{@response['token']}"}
    }
    puts "Deleting booking data with id #{@booking_id}"
    @response = HTTParty.send(method.downcase, "#{@api_url}#{relative_path}/#{@booking_id}", headers)
    puts "Here's the DELETE response:\n#{@response}"
  when 'POST'
    options = {
      body:{
        firstname: 'Jim',
        lastname: 'Brown',
        totalprice: 111,
        depositpaid: true,
        bookingdates: {
            checkin: '2018-01-01',
            checkout: '2019-01-01'
        },
        additionalneeds: 'Breakfast'
      }
    }
    puts "Here's the POST request body:\n#{options}"
    @response = HTTParty.send(method.downcase, "#{@api_url}#{relative_path}", options)
    puts "Here's the POST response body:\n#{@response.body}"
    @response_json = JSON.parse(@response.body)
    expect(@response_json['bookingid']).not_to eql('')
    @booking_id = @response_json['bookingid']
  end
end

Then(/^the response code should be "([^"]*)"$/) do |code|
  expect(@response.code).to eql(code.to_i)
end

And(/^the response should have body$/) do
  expect(@response_json['firstname']).not_to eql('')
  expect(@response_json['lastname']).not_to eql('')
  expect(@response_json['totalprice']).not_to eql('')
  expect(@response_json['depositpaid']).not_to eql('')
  expect(@response_json['bookingdates']).not_to eql('')
end

And(/^the data should be deleted$/) do
  @response = HTTParty.get("#{@api_url}/booking/#{@booking_id}")
  step 'the response code should be "404"'
end
