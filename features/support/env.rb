require 'watir'
require 'webdrivers'
require 'httparty'

Before('@amazon') do
  @browser = Watir::Browser.new :chrome
  @browser.window.maximize
end

Before('@api') do
  @api_url = 'https://restful-booker.herokuapp.com'
end

After('@amazon') do
  @browser.close
end